package com.kirillsv.shop.config;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.security.crypto.password.PasswordEncoder;

public class CustomUserPasswordEncoder implements PasswordEncoder {

	private static final String SALT = "MOI_SEKRETNI_KEY"; 
	@Override
	public String encode(CharSequence rawPassword) {
		return encrypt(rawPassword.toString());
	}

	public static String encrypt(String value) {

		if (value == null) {
			return value;
		}
		Key key = new SecretKeySpec(SALT.getBytes(), "AES");
		try {

			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return Base64.encodeBase64String(cipher.doFinal(value.getBytes()));
		} catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}

	public static String decrypt(String value) {

		if (value == null) {
			return value;
		}
		Key key = new SecretKeySpec(SALT.getBytes(), "AES");
		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key);
			return new String(cipher.doFinal(Base64.decodeBase64(value)));
		} catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
	    if (encode(rawPassword).equals(encodedPassword)) {
	        return true;
	    }
	    return false;
	}
}
