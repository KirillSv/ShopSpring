package com.kirillsv.shop.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
    public void addViewControllers(ViewControllerRegistry registry) { 
    	registry.addViewController("/").setViewName("/main");
        registry.addViewController("/security/login").setViewName("security/login");
        registry.addViewController("/security/logout").setViewName("security/logout"); 
        registry.addViewController("/aboutUs").setViewName("aboutUs");
        registry.addViewController("/errors/accessDenied").setViewName("/errors/accessDenied");
    }
    
}
