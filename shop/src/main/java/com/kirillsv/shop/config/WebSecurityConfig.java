package com.kirillsv.shop.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.kirillsv.shop.models.security.Role;
import com.kirillsv.shop.service.UserSevice;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserSevice userSevice;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable() // CHANGE!
				.authorizeRequests()
				.antMatchers("/", "/security/registration", "/showcase", "/aboutUs").permitAll()
			//	.antMatchers("/admin/**").hasRole(Role.ADMIN.toString())
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.loginPage("/security/login")
				.defaultSuccessUrl("/")
				.permitAll()
				.and()
				.logout()
				.logoutUrl("/security/logout")
				.invalidateHttpSession(true)
				.clearAuthentication(true)
				.deleteCookies("JSESSIONID")
				.permitAll()
				.and()
				.exceptionHandling()
				.accessDeniedPage("/errors/accessDenied");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userSevice).passwordEncoder(passwordEncoder());
	}

	@Bean
	protected PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
		// return new CustomUserPasswordEncoder();
	}

// **for add handled user without BD (del method  "configure")****************************

//	    @Bean  
//	    @Override
//	    protected UserDetailsService userDetailsService() {
//	      return new InMemoryUserDetailsManager(
//	    		  User.builder()        // user  from springSecurity.class ! 
//	    		  .username("admin")
//	    		  .password(passwordEncoder().encode("luboe_slovo"))
//	    		  .roles("ADMIN")
//	    		  .build()
//	    );
//	    }
}
