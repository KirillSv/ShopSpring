package com.kirillsv.shop.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.kirillsv.shop.models.Product;
import com.kirillsv.shop.models.Review;
import com.kirillsv.shop.models.security.User;
import com.kirillsv.shop.repositories.ProductRepository;
import com.kirillsv.shop.repositories.ReviewRepository;
import com.kirillsv.shop.repositories.UserRepository;

@Controller
//@PreAuthorize("hasRole('ADMIN')")
public class AdminController {
	@Autowired
	private ProductRepository pr;
	@Autowired
	private ReviewRepository rr;
	@Autowired
	private UserRepository ur;
	
	@GetMapping("/adminPage")
	public String adminPage(Model model) {
		model.addAttribute("title", "Администрирование");
		return "admin/adminMain";
	}
	
	@GetMapping("/adminPageProduct")
	public String productAdminPage(Model model) {
		model.addAttribute("title", "Управление товарами");
		Iterable<Product> products = pr.findAll();
		model.addAttribute("product", products);
		return "admin/adminPageProduct";
	}
	
	@GetMapping("/adminPageReview")
	public String showAllReview(Model model) {
		model.addAttribute("title", "Отзывы");
		ArrayList<Review> review = (ArrayList<Review>) rr.findAll();

		ArrayList<Review> thisReview = new ArrayList<>();
		for (Review rew : review) {
			if (!rew.isApproved()) {
				thisReview.add(rew);
			}

		}

		model.addAttribute("review", thisReview);
		return "admin/adminPageReview";
	}

	@GetMapping("/adminPageUser")
	public String showUserAdminPage(Model model) {
		model.addAttribute("title", "Управление юзерами");
		Iterable<User> users = ur.findAll();
		model.addAttribute("users", users);
		return "admin/adminPageUser";
	}
}
