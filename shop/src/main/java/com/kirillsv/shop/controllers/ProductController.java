package com.kirillsv.shop.controllers;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.kirillsv.shop.models.Product;
import com.kirillsv.shop.models.Review;
import com.kirillsv.shop.repositories.ProductRepository;
import com.kirillsv.shop.repositories.ReviewRepository;

@Controller
public class ProductController {
	@Autowired
	private ProductRepository pr;
	@Autowired
	private ReviewRepository rr;

	@PostMapping("product/review/add/{id}")
	public String addRewiev(@PathVariable(value = "id") long id, @RequestParam String review, Model model) {
		System.out.println(review + ' ' + id);
		Review rev = new Review(id, review);
		rr.save(rev);
		model.addAttribute("title", "Витрина");
		Iterable<Product> products = pr.findAll();
		model.addAttribute("product", products);
		return "showcase";

	}

	@GetMapping("/showcase")
	public String showcase(Model model) {
		model.addAttribute("title", "Витрина");
		Iterable<Product> products = pr.findAll();
		model.addAttribute("product", products);
		return "showcase";
	}

	@GetMapping("/product/{id}")
	public String blogDetails(@PathVariable(value = "id") long id, Model model) {
		if (!pr.existsById(id)) {
			return "redirect:/productDetails";
		}

		Optional<Product> product = pr.findById(id);
		ArrayList<Product> res = new ArrayList<>();
		product.ifPresent(res::add);
		model.addAttribute("product", res);

		ArrayList<Review> review = (ArrayList<Review>) rr.findAll();

		ArrayList<Review> thisReview = new ArrayList<>();
		for (Review rew : review) {
			if (rew.getProductId().equals(id) && rew.isApproved()) {
				thisReview.add(rew);
			}

		}
		model.addAttribute("review", thisReview);

		return "productDetails";
	}

	@GetMapping("/product/{id}/edit")
	public String productShowAllforUpdate(@PathVariable(value = "id") long id, Model model) {
		Product product = pr.findById(id).orElseThrow(IllegalStateException::new);
		model.addAttribute("product", product);
		return "productEdit";
	}

	@PostMapping("/product/{id}/edit")
	public String productUpdate(@PathVariable(value = "id") long id, @RequestParam String name,
			@RequestParam String description, @RequestParam Double price, Model model) {
		Product product = pr.findById(id).orElseThrow(IllegalStateException::new);
		product.setName(name);
		product.setDescription(description);
		product.setPrice(price);
		pr.save(product);

		Iterable<Product> products = pr.findAll();
		model.addAttribute("product", products);
		return "adminArea/adminPageProduct";
	}

	@GetMapping("/product/{id}/remove")
	public String blogPostRemove(@PathVariable(value = "id") long id, Model model) {
		Product product = pr.findById(id).orElseThrow(IllegalStateException::new);
		pr.delete(product);

		Iterable<Product> products = pr.findAll();
		model.addAttribute("product", products);
		return "adminArea/adminPageProduct";
	}

	@PostMapping("/product/add")
	public String addProduct(@RequestParam String name, @RequestParam String description, @RequestParam double price,
			Model model) {
		pr.save(new Product(name, description, price));
		return "redirect:/adminPageProduct";
	}
}
