package com.kirillsv.shop.controllers;

import java.util.Collections;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.kirillsv.shop.models.security.Role;
import com.kirillsv.shop.models.security.User;
import com.kirillsv.shop.repositories.UserRepository;
@Controller
public class RegistrationController {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @GetMapping("security/registration")
    public String registration(Map<String, Object> model) {
    	 model.put("message", "You need to fill in the registration data.");
        return "security/registration";
    }

    @PostMapping("security/registration")
    public String addUser(User user, Map<String, Object> model, String username) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb != null && userFromDb.getUsername().equalsIgnoreCase(username)) {
            model.put("message", "User exists!");
            userFromDb=null;
            return "security/registration";
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        final String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        userRepo.save(user);
        
        model.put("message", "User registration successfull !");
        return "security/registration";
    }
}
