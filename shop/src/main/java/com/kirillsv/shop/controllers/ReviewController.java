package com.kirillsv.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.kirillsv.shop.models.Review;
import com.kirillsv.shop.repositories.ReviewRepository;

@Controller
public class ReviewController {
	@Autowired
	private ReviewRepository rr;


	@GetMapping("/review/approwed/{id}")
	public String approwedReview(@PathVariable(value = "id") long id, Model model) {
		Review review = rr.findById(id).orElseThrow(IllegalStateException::new);
		review.setApproved(true);
		rr.save(review);
		Iterable<Review> reviews = rr.findAll();
		model.addAttribute("review", reviews);
		return "redirect:/adminPageReview";
	}

	@GetMapping("/review/remove/{id}")
	public String removeReview(@PathVariable(value = "id") long id, Model model) {
		Review review = rr.findById(id).orElseThrow(IllegalStateException::new);
		rr.delete(review);
		Iterable<Review> reviews = rr.findAll();
		model.addAttribute("review", reviews);
		return "redirect:/adminPageReview";
	}

}
