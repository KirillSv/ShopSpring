package com.kirillsv.shop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.kirillsv.shop.models.security.User;
import com.kirillsv.shop.repositories.UserRepository;

@Controller
//@RequestMapping("/user")
//@PreAuthorize("hasAuthority('ADMIN')") //чтобы она заработала надо добавить  в вебконфиг ENABLEGLOBALSECURITY
public class UserController {
	@Autowired
	private UserRepository userRepo;

	@GetMapping("/user/{user}/remove")
	public String userRemove(@PathVariable User user, Model model) {
		userRepo.delete(user);
		model.addAttribute("users", userRepo.findAll());
		return "admin/adminPageUser";
	}

	@GetMapping("/user/{user}/edit")
	public String editUser(@PathVariable User user, Model model) {
		user.setActive(!user.isActive()) ;
		userRepo.save(user);
		model.addAttribute("users", userRepo.findAll());
		return "admin/adminPageUser";
	}

//	  @PostMapping
//	    public String userSave(
//	            @RequestParam String username,
//	            @RequestParam Map<String, String> form,
//	            @RequestParam("userId") User user
//	    ) {
//	        user.setUsername(username);
//
//	        Set<String> roles = Arrays.stream(Role.values())
//	                .map(Role::name)
//	                .collect(Collectors.toSet());
//
//	        user.getRoles().clear();
//
//	        for (String key : form.keySet()) {
//	            if (roles.contains(key)) {
//	                user.getRoles().add(Role.valueOf(key));
//	            }
//	        }
//
//	        userRepo.save(user);
//
//	        return "redirect:/user";
//	    }
}
