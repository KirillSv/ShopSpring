package com.kirillsv.shop.models;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Review {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	Long productId;
	Date data;
	Long userId;
	String text;
	boolean approved;

	public Review() {
	}

	public Review(Long productId, String text) {
		data = new java.sql.Date(System.currentTimeMillis());
		this.productId=productId;
		this.userId = (long)1;                                        //change when will be security
		this.text = text;
		approved=false;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Review [id=" + id + ", productId=" + productId + ", data=" + data + ", userId=" + userId + ", text="
				+ text + "]";
	}

}
