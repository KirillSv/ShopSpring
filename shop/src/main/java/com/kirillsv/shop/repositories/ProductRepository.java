package com.kirillsv.shop.repositories;

import org.springframework.data.repository.CrudRepository;
import com.kirillsv.shop.models.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

}
