package com.kirillsv.shop.repositories;

import org.springframework.data.repository.CrudRepository;

import com.kirillsv.shop.models.Review;

public interface ReviewRepository extends CrudRepository<Review, Long> {

}
