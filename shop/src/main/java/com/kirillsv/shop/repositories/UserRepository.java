package com.kirillsv.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kirillsv.shop.models.security.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
