package com.kirillsv.shop.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AdminAccessTest {

	@Autowired
	private AdminController ac;
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void adminPageExistTest() throws Exception {
		assertThat(ac).isNotNull();
	}
	
	@Test
	public void enterAdminArreaWhisoutAuthentTest() throws Exception {
		this.mockMvc.perform(get("/adminPage"))
		.andDo(print())
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl("http://localhost/security/login"));
	}
	
	@Test
	@WithUserDetails("admin")
	public void enterAdminToAdminPage() throws Exception {
		this.mockMvc.perform(get("/adminPage"))
		.andDo(print())
		.andExpect(status()
		.isOk())
		.andExpect(content().string(containsString("Зона Администратора")));
	}
	
	@Test
	@WithUserDetails("1")
	public void enterUserToAdminPage() throws Exception {
		this.mockMvc.perform(get("/adminPage"))
		.andDo(print())
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl("http://localhost/errors/accessDenied"));
	}
	

}